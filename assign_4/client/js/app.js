function savedata() {
    var username = document.getElementById('uname').value;
    var password = document.getElementById('pwd').value;

    if (username.length === 0) {
        document.getElementById('error_uname').innerHTML = "please enter user name"
    }

    if (password.length === 0) {
        document.getElementById('error_pass').innerHTML = "please enter password"
    }

    var myObj = { 'name': username, 'pass': password }
    request_data(myObj, "POST");

}
function deleteRow(id) {
    request_data(id, "DELETE")

}
function request_data(para = "", type = "GET") {
    var table = document.getElementById("myTable");
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {

                if (type != "DELETE") {
                    var myObj = JSON.parse(this.responseText);
                    myObj.forEach((ele1) => {
                        create_table(table, ele1);
                    });
                }
                else{

                }
            }
            if (this.status != 200) {
                document.getElementById('page_error').innerHTML = "DATA NOT ADDED";
            }
        }
    }

    if (type != 'DELETE') { xhttp.open(type, "http://localhost:3010", true); }
    else { xhttp.open(type, `http://localhost:3010/${para}`, true); }
    xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    if (type === "POST") {
        xhttp.send(JSON.stringify(para));
    }
    else {
        xhttp.send();
    }

}

function create_table(table, record) {
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = record.id_login;
    cell2.innerHTML = record.uname_login;
    cell3.innerHTML = record.pass_login;
    cell4.innerText = `<button onClick=deleteRow(${record.id_login})  id='record.id_login'>delete</button>`;
}

function reset() {
    document.getElementById("myForm").reset();
} 