function savedata() {
    const uname = document.getElementById('username').value;
    const pwd = document.getElementById('password').value;
   
    var myObj = { 'name': uname, 'password': pwd }
    request_data(myObj, "POST", "login");
  }
  
  function request_data(para = "", type = "GET", url = "") {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        //document.getElementById('user_message').innerHTML = "";
        if (this.status === 401) {
          alert("UnAuthorized");
        }
        if (this.status == 200) {
          
            var myObj = JSON.parse(this.responseText);
            myObj.forEach((record) => {
              CheckData(record);
            });
          
          
        }
        else {
        //   document.getElementById('page_error').innerHTML = "error occured..";
        }
      }
      else {
        // document.getElementById('user_message').innerHTML = "loading....";
      }
    }
    xhttp.open(type, `http://localhost:3000/${url}`, true);//true is for async , false for sync
    xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    if (type === "POST") {
      xhttp.send(JSON.stringify(para));
    }
    else {
      xhttp.send();
    }
  }
  function CheckData(record) {
    window.location.href = "./Wow_web.html";
  }