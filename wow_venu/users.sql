CREATE TABLE `mylibray`.`users` (
  `Userid` INT NOT NULL AUTO_INCREMENT,
  `Role` VARCHAR(15) NOT NULL,
  `Email` VARCHAR(30) NOT NULL,
  `Username` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(15) NOT NULL,
  `Name` VARCHAR(15) NOT NULL,
  `Gender` VARCHAR(1) NOT NULL,
  `Age` INT(2) NOT NULL,
  `Address` VARCHAR(45) NOT NULL,
  `State` VARCHAR(15) NOT NULL,
  `City` VARCHAR(15) NOT NULL,
  `Idtype` VARCHAR(15) NULL,
  `Idphoto` BLOB NULL,
  `Createtimestamp` DATETIME NULL,
  UNIQUE INDEX `Username_UNIQUE` (`Username` ASC) VISIBLE,
  PRIMARY KEY (`Userid`));
Alter table `users`
change column `Createtimestamp` `Createtimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP; 