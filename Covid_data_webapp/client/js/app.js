function request_data() {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            // Response from server is stored in myObj
            var myObj = JSON.parse(this.responseText);
            //getting value from the input text box from html
            var txtcity = document.getElementById('txtcity').value;
            var cityfound;
            //program to find city 
            myObj.areas.forEach(ele1 => {
                ele1.areas.forEach(ele2 => {
                    ele2.areas.forEach(ele3 => {
                        if (ele3.displayName === txtcity) {
                            cityfound = ele3
                        }

                    })
                })
            })
            //displaying data found 
            if (cityfound != undefined) {
                document.getElementById("id").innerHTML = cityfound.id;
                document.getElementById("c_name").innerHTML = cityfound.displayName;
                document.getElementById("areas").innerHTML = cityfound.areas;
                document.getElementById("conf_cases").innerHTML = cityfound.totalConfirmed;

                document.getElementById("recovered").innerHTML = cityfound.totalRecovered;
                document.getElementById("latitude").innerHTML = cityfound.lat;
                document.getElementById("longitude").innerHTML = cityfound.long;
                document.getElementById("state_country").innerHTML = cityfound.parentId;
            }
            else {
                //if city name is not found
                alert("city not found")
            }
        }
    };
    // extracting data from local host
    xhttp.open("GET", "http://localhost:3000", true);
    xhttp.send();
}