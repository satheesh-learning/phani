function savedata() {
    //alert("Bhoo! Ya..")


    var username = document.getElementById('uname').value;
    var password = document.getElementById('pwd').value;

    if (username.length === 0) {
        document.getElementById('error_uname').innerHTML = "please enter user name"
    }

    if (password.length === 0) {
        document.getElementById('error_pass').innerHTML = "please enter password"
    }

    var myObj = { 'name': username, 'pass': password }
    //request_data(); write it in onload function
    request_data(myObj, "POST");
    request_data();
}
function request_data(para = "", type = "GET") {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                //alert(this.responseText);
                if (type === "GET") {
                    var obj;
                    var myObj = JSON.parse(this.responseText);
                    //alert(myObj)
                    
                    var table = document.getElementById("myTable");
                    myObj.forEach((ele1,index) => {
                        console.log(ele1);
                        create_table(table,ele1,index);
                    });

                }
                if(this.status!=200) {
                    document.getElementById('page_error').innerHTML = "details not found";
                }
            }
        }
    }
    xhttp.open(type, "http://localhost:3010", true);
    xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    if (type === "POST")
        xhttp.send(JSON.stringify(para));
    else
        xhttp.send();
}
function create_table(table,record,index) {
    
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);


    cell1.innerHTML = index;
    cell2.innerHTML = record.uname_login;
    cell3.innerHTML = record.pass_login;
}